<?php 

class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName  = $lastName;
	}
	public function printName(){
		return "your full name is $this->firstName $this->lastName";
	}
}
$person = new Person("Senku", "sample", "Isagami");

class Developer extends Person {

	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a Developer";
	}
}
$developer = new Developer("John","Finch","Smith");

class Engineer extends Person {
	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are an Engineer";
	}
}
$engineer = new Engineer("Harold", "Myers", "Reese");

